import { Injectable, OnInit } from '@angular/core';
import Piatto from "./piatto"

@Injectable({
    providedIn: 'root'
})
export class StrapiService implements OnInit {

    private readonly queryUrl = "http://localhost:1337/api/piatti?populate[0]=immagine&populate[1]=categoria&populate[2]=allergeni.icona"
    startUrl: string = "http://localhost:1337"
    private plates: Piatto[] | undefined

    constructor() { }

    async ngOnInit(): Promise<void> {
        this.plates = await fetch(this.queryUrl).then(res => res.json()).then(json => json.data)
    }

    async getMenu(): Promise<Piatto[] | undefined> {
        if(!this.plates) {
            try {
                this.plates = await fetch(this.queryUrl).then(res => res.json()).then(json => json.data)
            } catch (error) {
                throw new Error("Impossibile recuperare il menù")
            }
        }
        return this.plates
    }
}
