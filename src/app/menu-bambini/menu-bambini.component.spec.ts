import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuBambiniComponent } from './menu-bambini.component';

describe('MenuBambiniComponent', () => {
  let component: MenuBambiniComponent;
  let fixture: ComponentFixture<MenuBambiniComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuBambiniComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuBambiniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
