import { Component, OnInit } from '@angular/core';
import { StrapiService } from '../strapi.service';
import Piatto from '../piatto';

@Component({
  selector: 'app-menu-bambini',
  templateUrl: './menu-bambini.component.html',
  styleUrls: ['./menu-bambini.component.css']
})
export class MenuBambiniComponent implements OnInit {

  plates: Piatto[] = []
    allergeni: any[] = []
    startUrl: string = ""
    constructor(private service: StrapiService) {

    }

    async ngOnInit() {
      this.plates = (await this.service.getMenu())!.filter(plate => {
        return (plate.attributes.tipoMenu === "Menù bambini" &&
        plate.attributes.categoria.data.attributes.nome != "Dolci" &&
        plate.attributes.categoria.data.attributes.nome != "Bevande")
    })
        this.startUrl = this.service.startUrl
    }


}
