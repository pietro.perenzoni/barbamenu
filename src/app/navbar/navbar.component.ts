import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

    menuOpen = false
    mobile = false

    constructor() { }

    ngOnInit(): void {
        if (window.innerWidth < 768) {
            this.mobile = true
        }
        window.onresize = () => {
            if (window.innerWidth < 768) {
                this.mobile = true
            } else {
                this.mobile = false
            }
        }
    }

}
