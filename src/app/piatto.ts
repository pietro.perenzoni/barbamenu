export default interface Piatto {
    id: number
    attributes: {
        nome: string,
        prezzo: number,
        tipoMenu: "Menù" | "Menù del giorno" | "Menù bambini",
        traduzione: string,
        createdAt: string,
        publishedAt: string,
        updatedAt: string,
        categoria: {
            id: number,
            data: {
                attributes: {
                    nome: string,
                    createdAt: string,
                    publishedAt: string,
                    updatedAt: string
                }
            }
        },
        allergeni: {
            data: [
                {
                    id: number,
                    attributes:{
                        nome: string,
                        createdAt: string,
                        publishedAt: string,
                        updatedAt: string,
                        icona:{
                            data: Immagine
                        }
                    }
                }
            ]
        },
        immagine: {
            data: Immagine
        }
    }
}

export interface Immagine {
    id: number,
    attributes: {
        createdAt: string,
        publishedAt: string,
        updatedAt: string,
        alternativeText: string,
        caption: string,
        ext: string,
        formats: string,
        hash: string,
        height: number,
        width: number,
        mime: string,
        name: string,
        previewurl: string,
        provider: string,
        provider_metadata: string,
        size: number,
        url: string
    }
}