import { Component, OnInit } from '@angular/core';
import { StrapiService } from '../strapi.service';
import Piatto from '../piatto';

@Component({
    selector: 'app-bevande',
    templateUrl: './bevande.component.html',
    styleUrls: ['./bevande.component.css']
})
export class BevandeComponent implements OnInit {

    plates: Piatto[] = []
    allergeni: any[] = []
    startUrl: string = ""
    constructor(private service: StrapiService) {

    }

    async ngOnInit() {
        this.plates = (await this.service.getMenu())!.filter(plate => {
            return (plate.attributes.categoria.data.attributes.nome === "Bevande" &&
                    plate.attributes.tipoMenu != "Menù del giorno")
        })
        this.startUrl = this.service.startUrl
    }

}
