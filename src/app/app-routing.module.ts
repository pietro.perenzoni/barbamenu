import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BevandeComponent } from './bevande/bevande.component';
import { ChiSiamoComponent } from './chi-siamo/chi-siamo.component';
import { DolciComponent } from './dolci/dolci.component';
import { MenuBambiniComponent } from './menu-bambini/menu-bambini.component';
import { MenuDelGiornoComponent } from './menu-del-giorno/menu-del-giorno.component';
import { MenuFissoComponent } from './menu-fisso/menu-fisso.component';

const routes: Routes = [
  {path: "", component: ChiSiamoComponent},
  {path: "menu-giornaliero", component: MenuDelGiornoComponent},
  {path: "menu-fisso", component: MenuFissoComponent},
  {path: "menu-bambini", component: MenuBambiniComponent},
  {path: "dolci", component: DolciComponent},
  {path: "bevande", component: BevandeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
