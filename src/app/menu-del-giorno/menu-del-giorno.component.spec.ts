import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuDelGiornoComponent } from './menu-del-giorno.component';

describe('MenuDelGiornoComponent', () => {
  let component: MenuDelGiornoComponent;
  let fixture: ComponentFixture<MenuDelGiornoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuDelGiornoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuDelGiornoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
