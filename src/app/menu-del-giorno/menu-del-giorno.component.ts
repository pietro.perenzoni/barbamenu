import { Component, OnInit } from '@angular/core';
import { StrapiService } from '../strapi.service';
import Piatto from '../piatto';

@Component({
  selector: 'app-menu-del-giorno',
  templateUrl: './menu-del-giorno.component.html',
  styleUrls: ['./menu-del-giorno.component.css']
})
export class MenuDelGiornoComponent implements OnInit {

    plates: Piatto[] = []
    allergeni: any[] = []
    startUrl: string = ""
    constructor(private service: StrapiService) {

    }

    async ngOnInit() {
      this.plates = (await this.service.getMenu())!.filter(plate => {
        return plate.attributes.tipoMenu === "Menù del giorno"
    })
        this.startUrl = this.service.startUrl
    }

}
