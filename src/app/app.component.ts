import { Component, OnInit } from '@angular/core';
import { StrapiService } from './strapi.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    title = 'BarbaMenu';

    constructor(public service: StrapiService) {}

    async ngOnInit(): Promise<void> {
        //this.piatti = await fetch("http://localhost:1337/api/piatti?populate[0]=immagine&populate[1]=categoria&populate[2]=allergeni.icona").then(res => res.json())
        //console.log(this.piatti)
        // this.show = true
        //console.log(this.service.piatti)
        console.log(await this.service.getMenu())
    }
}
