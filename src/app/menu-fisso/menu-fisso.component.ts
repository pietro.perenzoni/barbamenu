import { Component, OnInit } from '@angular/core';
import { StrapiService } from '../strapi.service';
import Piatto from '../piatto';

@Component({
    selector: 'app-menu-fisso',
    templateUrl: './menu-fisso.component.html',
    styleUrls: ['./menu-fisso.component.css']
})
export class MenuFissoComponent implements OnInit {

    plates: Piatto[] = []
    allergeni: any[] = []
    startUrl: string = ""
    constructor(private service: StrapiService) {

    }

    async ngOnInit() {
        this.plates = (await this.service.getMenu())!.filter(plate => {
            return (plate.attributes.tipoMenu === "Menù" &&
            plate.attributes.categoria.data.attributes.nome != "Dolci" &&
            plate.attributes.categoria.data.attributes.nome != "Bevande")
        })
        this.startUrl = this.service.startUrl
    }
}
