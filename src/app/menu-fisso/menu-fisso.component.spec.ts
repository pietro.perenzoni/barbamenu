import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuFissoComponent } from './menu-fisso.component';

describe('MenuFissoComponent', () => {
  let component: MenuFissoComponent;
  let fixture: ComponentFixture<MenuFissoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuFissoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuFissoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
