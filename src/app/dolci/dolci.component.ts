import { Component, OnInit } from '@angular/core';
import { StrapiService } from '../strapi.service';
import Piatto from '../piatto';

@Component({
  selector: 'app-dolci',
  templateUrl: './dolci.component.html',
  styleUrls: ['./dolci.component.css']
})
export class DolciComponent implements OnInit {

  plates: Piatto[] = []
    allergeni: any[] = []
    startUrl: string = ""
    constructor(private service: StrapiService) {

    }

    async ngOnInit() {
      this.plates = (await this.service.getMenu())!.filter(plate => {
        return (plate.attributes.categoria.data.attributes.nome === "Dolci" &&
                plate.attributes.tipoMenu != "Menù del giorno")
        })
        this.startUrl = this.service.startUrl
    }

}
