import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ChiSiamoComponent } from './chi-siamo/chi-siamo.component';
import { MenuDelGiornoComponent } from './menu-del-giorno/menu-del-giorno.component';
import { MenuFissoComponent } from './menu-fisso/menu-fisso.component';
import { DolciComponent } from './dolci/dolci.component';
import { MenuBambiniComponent } from './menu-bambini/menu-bambini.component';
import { BevandeComponent } from './bevande/bevande.component';
import { StrapiService } from './strapi.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ChiSiamoComponent,
    MenuDelGiornoComponent,
    MenuFissoComponent,
    DolciComponent,
    MenuBambiniComponent,
    BevandeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [StrapiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
