module.exports = {
  content: ["./src/**/*.{html,ts}"],
  theme: {
    extend: {
      colors: {
        primary: "#800a21",
        secondary: "#af0e24"
      },
      backgroundImage: {
        spaghetti: "url('/assets/spaghetti.svg')",
        locanda: "url('/assets/locanda.svg')"
      },
      keyframes: {
        rise: {
          "100%": {
            transform: "translateX(0)"
          }
        }
      },
      animation: {
        "spaghetti-rise": "3s ease-in-out 0.75s 1 normal forwards running rise"
      },
      screens: {
        xs: "512px"
      }
    },
  },
  plugins: [],
}
